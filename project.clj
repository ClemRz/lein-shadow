(defproject lein-shadow "0.3.1"
  :description "A Leiningen plugin to help keep your project configuration in your project.clj file when using shadow-cljs"
  :url "https://gitlab.com/nikperic/lein-shadow"
  :license {:name "MIT License"
            :url  "https://opensource.org/licenses/MIT"}
  :dependencies [[meta-merge            "1.0.0"]
                 [org.clojure/data.json "1.0.0"]]
  :eval-in-leiningen true)
